package recipeapp.cpsc430.recipeandroidapp.classes;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import recipeapp.cpsc430.recipeandroidapp.search.RecipeCard;

/**
 * HelperSharedPreferences manages the app's persistent data within the phone's memory storage.
 */

public class HelperSharedPreferences {

    public static class SharedPreferencesKeys {
        public static final String favoritesKey = "favoritesKey";
    }

    public static void putSharedPreferencesInt(Context context, String key, int value){
        SharedPreferences preferences= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=preferences.edit();
        edit.putInt(key, value);
        edit.apply();
    }

    public static void putSharedPreferencesBoolean(Context context, String key, boolean val){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=preferences.edit();
        edit.putBoolean(key, val);
        edit.apply();
    }

    public static void putSharedPreferencesString(Context context, String key, String val){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=preferences.edit();
        edit.putString(key, val);
        edit.apply();
    }

    public static void putSharedPreferencesStringSet(Context context, String key, ArrayList<String> arrayList) {
        SharedPreferences prefs=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=prefs.edit();
        Set<String> set = new HashSet<String>();
        set.addAll(arrayList);
        edit.putStringSet(key, set);
        edit.apply();
    }

    public static void putSharedPreferencesFloat(Context context, String key, float val){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=preferences.edit();
        edit.putFloat(key, val);
        edit.apply();
    }

    public static void putSharedPreferencesLong(Context context, String key, long val){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit=preferences.edit();
        edit.putLong(key, val);
        edit.apply();
    }

    public static long getSharedPreferencesLong(Context context, String key, long _default){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getLong(key, _default);
    }

    public static float getSharedPreferencesFloat(Context context, String key, float _default){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getFloat(key, _default);
    }

    public static String getSharedPreferencesString(Context context, String key, String _default){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, _default);
    }

    public static ArrayList<String> getSharedPreferencesStringSet(Context context, String key) {
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> set = preferences.getStringSet(key, null);
        List<String> list;

        if (set == null) {
            list = new ArrayList<>();
        }
        else {
            list = new ArrayList<>(set);
        }

        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.addAll(list);
        return arrayList;
    }

    public static int getSharedPreferencesInt(Context context, String key, int _default){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(key, _default);
    }

    public static boolean getSharedPreferencesBoolean(Context context, String key, boolean _default){
        SharedPreferences preferences=PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getBoolean(key, _default);
    }
}
