package recipeapp.cpsc430.recipeandroidapp.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;
import recipeapp.cpsc430.recipeandroidapp.classes.HelperSharedPreferences;

/**
 * SearchHolder contains the UI for each recipe card to be displayed on the page from the query results.
 */

public class SearchHolder extends RecyclerView.ViewHolder {

    private TextView nameLabel;
    private TextView categoryLabel;
    private ImageView imageView;
    private Button favoriteButton;

    public SearchHolder(View itemView) {
        super(itemView);

        this.nameLabel = itemView.findViewById(R.id.nameLabel);
        this.categoryLabel = itemView.findViewById(R.id.categoryLabel);
        this.imageView = itemView.findViewById(R.id.imageView);
        this.favoriteButton = itemView.findViewById(R.id.favoriteButton);

    }

    public void updateUI(final RecipeCard recipeCard, final Context c) {

        nameLabel.setText(recipeCard.getTitle());
        categoryLabel.setText(recipeCard.getCategory());
        imageView.setImageBitmap(recipeCard.getImage());

        favoriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ArrayList<String> recipes = HelperSharedPreferences.getSharedPreferencesStringSet(c, HelperSharedPreferences.SharedPreferencesKeys.favoritesKey);

                recipes.add(recipeCard.getTitle());

                HelperSharedPreferences.putSharedPreferencesStringSet(c, HelperSharedPreferences.SharedPreferencesKeys.favoritesKey, recipes);

                Toast.makeText(c, recipeCard.getTitle() + " has been put into your favorites list!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}