package recipeapp.cpsc430.recipeandroidapp.search;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;

public class SearchDetail extends AppCompatActivity {

    private TextView titleLabel;
    private TextView categoryLabel;
    private ImageView detailImageView;
    private TextView ingredientLabel;

    private RecipeCard recipeCard;
    private int index;
    private int selection;
    private Bitmap bitmap;
    private ArrayList<String> ingredients;


    private void updateUI() {

        titleLabel.setText(recipeCard.getTitle());
        categoryLabel.setText(recipeCard.getCategory());
        detailImageView.setImageBitmap(bitmap);

        String ingredientStr = "";

        for (String ingredient : ingredients) {
            ingredientStr += ingredient + "\n\n";
        }

        ingredientLabel.setText(ingredientStr);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_detail);

        titleLabel = findViewById(R.id.titleLabel);
        categoryLabel = findViewById(R.id.categoryLabel);
        detailImageView = findViewById(R.id.detailImageView);
        ingredientLabel = findViewById(R.id.ingredientLabel);

        recipeCard = getIntent().getParcelableExtra("item");
        index = getIntent().getIntExtra("index", 0);
        selection = getIntent().getIntExtra("selection", 0);
        bitmap = getIntent().getParcelableExtra("image");
        ingredients = getIntent().getStringArrayListExtra("ingredients");

        updateUI();
    }

}
