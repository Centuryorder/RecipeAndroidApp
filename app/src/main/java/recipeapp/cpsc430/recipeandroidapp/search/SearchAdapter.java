package recipeapp.cpsc430.recipeandroidapp.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;

/**
 * SearchAdapter contains the layout that holds the recipe card objects that will be displayed to the user from their search results.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchHolder> {

    private ArrayList<RecipeCard> items;
    private Context context;
    private int selection;

    public SearchAdapter(ArrayList<RecipeCard> items, Context context, int selection) {

        this.items = items;
        this.context = context;
        this.selection = selection;
    }

    @Override
    public SearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemCard = LayoutInflater.from(context)
                .inflate(R.layout.search_page_row, parent, false);
        return new SearchHolder(itemCard);
    }

    @Override
    public void onBindViewHolder(final SearchHolder holder, final int position) {

        final RecipeCard item = items.get(position);
        holder.updateUI(item, context);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SearchPage.getSearchActivity().loadSearchDetailActivity(item, position, 0);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}