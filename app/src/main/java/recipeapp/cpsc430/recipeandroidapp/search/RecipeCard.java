package recipeapp.cpsc430.recipeandroidapp.search;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * RecipeCard is the object that is displayed when a user searches for recipes. These cards contain a title, category, image, and an arraylist of ingredients.
 */

public class RecipeCard implements Parcelable {

    private String title;
    private Bitmap imageURL;
    private String category;
    private ArrayList<String> ingredients;

    public RecipeCard(String title, Bitmap imageURL, String category) {
        this.title = title;
        this.imageURL = imageURL;
        this.category = category;
        this.ingredients = new ArrayList<>();
    }

    private RecipeCard(Parcel in) {

        this.title = in.readString();
        this.category = in.readString();
    }

    public String getTitle() {
        return title;
    }

    public Bitmap getImage() {
        return imageURL;
    }

    public String getCategory() {
        return category;
    }

    public String getIngredient(int index) {return ingredients.get(index);}

    public ArrayList<String> getIngredientArray() { return ingredients; }

    public void addIngredient(String ingredient) {this.ingredients.add(ingredient);}

    public void setTitle(String title) {
        this.title = title;
    }

    public void setImage(Bitmap imageName) {
        this.imageURL = imageName;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public static final Parcelable.Creator<RecipeCard> CREATOR = new Parcelable.Creator<RecipeCard>() {
        public RecipeCard createFromParcel(Parcel in) {
            return new RecipeCard(in);
        }

        public RecipeCard[] newArray(int size) {
            return new RecipeCard[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(category);
    }
}
