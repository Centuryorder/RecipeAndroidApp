package recipeapp.cpsc430.recipeandroidapp.search;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;
import recipeapp.cpsc430.recipeandroidapp.shopping_list.ItemDecorator;
import recipeapp.cpsc430.recipeandroidapp.shopping_list.ShoppingList;
import recipeapp.cpsc430.recipeandroidapp.shopping_list.ShoppingListAdapter;
import recipeapp.cpsc430.recipeandroidapp.shopping_list.ShoppingListItem;

/**
 * SearchPage is the activity that displays the search page and results to the user when active. It allows the user to enter a string
 * to search for a recipe and then JSON data is pulled from an API and displayed in a graphical format via RecipeCard objects.
 */

public class SearchPage extends AppCompatActivity implements View.OnClickListener {

    private String query;
    private Button submit;
    private ArrayList <RecipeCard> recipeArr = new ArrayList<>();

    private SearchAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    AutoCompleteTextView search_box;

    private static SearchPage searchPageActivity;

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
                @Override
                public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    return false;
                }

                /** handle swiping of recipe card objects to add to the shopping list **/
                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    RecipeCard recipe = recipeArr.get(viewHolder.getAdapterPosition());
                    ArrayList<String> addIngredients = recipe.getIngredientArray();
                    for(int i = 0; i < addIngredients.size(); i++) {
                        ShoppingList.shoppingListItems.add(new ShoppingListItem(addIngredients.get(i)));
                    }
                    adapter.notifyItemChanged(viewHolder.getAdapterPosition());
                    String displayMessage = recipe.getTitle() + " added to your shopping list.";
                    Toast toast = Toast.makeText(getApplicationContext(), displayMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }
    };

    public void loadSearchDetailActivity(RecipeCard recipeCard, int index, int selection) {

        Intent intent = new Intent(this, SearchDetail.class);
        intent.putExtra("item", recipeCard);
        intent.putExtra("index", index);
        intent.putExtra("selection", selection);
        intent.putExtra("image", recipeCard.getImage());
        intent.putExtra("ingredients", recipeCard.getIngredientArray());
        startActivity(intent);
    }

    /**
     * Returns the search page activity.
     * @return Search_Page activity
     */
    public static SearchPage getSearchActivity() {
        return searchPageActivity;
    }

    /**
     * Sets the array list adapter with correct items.
     */
    private void updateItems(int selection) {

        adapter = new SearchAdapter(recipeArr, this, selection);
        recyclerView.setAdapter(adapter);
    }

    private class JsonTask extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... params) {

            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();

                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line = "";

                while ((line = reader.readLine()) != null) {
                    buffer.append(line+"\n");
                }

                return buffer.toString();
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
            finally {

                if (connection != null) {
                    connection.disconnect();
                }
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        /** perform the JSON pull from the API and create the recipe cards with the data **/
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ArrayList<Ingredient> ingredients = new ArrayList<>();
            String name = "";
            String category = "";
            ArrayList<String> categoryArr = new ArrayList<>();
            String imageUrl = "";

            try {
                JSONObject response = new JSONObject(result);
                JSONArray hits = response.getJSONArray("hits");
                
                // if no results are found, tell the user
                if(hits.length() == 0) {
                    String displayMessage = "No results found for " +  query + ".";
                    Toast toast = Toast.makeText(getApplicationContext(), displayMessage, Toast.LENGTH_SHORT);
                    toast.show();
                }

                for(int i =0; i < hits.length(); i++) {
                    JSONObject h = hits.getJSONObject(i);
                    JSONObject r = h.getJSONObject("recipe");
                    name = r.getString("label");
                    imageUrl = r.getString("image");
                    category = String.valueOf(r.getJSONArray("healthLabels").get(0)) + ", " + String.valueOf(r.getJSONArray("healthLabels").get(1));

                    RecipeCard newRecipe = new RecipeCard(name, null, category);
                    recipeArr.add(newRecipe);

                    new DownloadImageTask(newRecipe).execute(imageUrl);

                    JSONArray ing = r.getJSONArray("ingredients");

                    for (int j = 0; j < ing.length(); j++) {
                        JSONObject ingrl = ing.getJSONObject(j);
                        String text = ingrl.getString("text");
                        newRecipe.addIngredient(text);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /** provide each recipe card with the image from the API **/
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

        RecipeCard recipe;

        public DownloadImageTask(RecipeCard recipe) {
            this.recipe = recipe;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {

            recipe.setImage(result);

            updateItems(0);
        }
    }

    private void clearSearchResults() {
        recipeArr.clear();
    }


    /** get the text from the search box and use it for the query **/
    public void onClick(View view) {
        query = search_box.getText().toString();
        clearSearchResults();
        // perform the query
        new JsonTask().execute("https://api.edamam.com/search?q=" + query + "&app_id=bd1e0302&app_key=927dfba02f127c67fd2edb450fca59f5");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    public void onBackPressed()
    {
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_);
        search_box = (AutoCompleteTextView) findViewById(R.id.searchField);
        search_box.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    onClick(textView);
                    return true;
                }
                return false;
            }
        });
        // get the submit button and create a listener for clicks/taps
        submit = (Button) findViewById(R.id.searchButton);
        submit.setOnClickListener(this);

        // Set the back button
        try { getSupportActionBar().setDisplayHomeAsUpEnabled(true); }
        catch (Exception e) { Log.i("Exception", e.getLocalizedMessage()); }

        searchPageActivity = this;

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = findViewById(R.id.searchRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new ItemDecorator(0, 0, 0, 10));

        // attach the itemtouchhelper to the recycler view to allow motion gestures like swiping
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }
}
