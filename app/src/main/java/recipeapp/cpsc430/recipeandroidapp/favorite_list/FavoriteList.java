package recipeapp.cpsc430.recipeandroidapp.favorite_list;

/**
 * FavoriteList is the activity that allows the favorite page to be displayed to the user.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;
import recipeapp.cpsc430.recipeandroidapp.classes.HelperSharedPreferences;

public class FavoriteList extends AppCompatActivity {

    private ListView listView;
    private ArrayAdapter<String> adapter;


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite_list);

        // Set back button
        try { getSupportActionBar().setDisplayHomeAsUpEnabled(true); }
        catch (Exception e) { Log.i("Exception", e.getLocalizedMessage()); }

        listView = findViewById(R.id.listView);

        ArrayList<String> recipes = HelperSharedPreferences.getSharedPreferencesStringSet(this, HelperSharedPreferences.SharedPreferencesKeys.favoritesKey);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, recipes);
        listView.setAdapter(adapter);
    }
}