package recipeapp.cpsc430.recipeandroidapp.main_menu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import recipeapp.cpsc430.recipeandroidapp.R;
import recipeapp.cpsc430.recipeandroidapp.favorite_list.FavoriteList;
import recipeapp.cpsc430.recipeandroidapp.search.SearchPage;
import recipeapp.cpsc430.recipeandroidapp.shopping_list.ShoppingList;

/** MainMenu is the first screen after logging in that the user is presented with. From here they can navigate to all of the other
 *  pages: searchpage, favoritelist, shoppinglist, and they can exit the app.
 */

public class MainMenu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        Button vSearchPage  = (Button) findViewById(R.id.SearchPageButton);
        Button vFavoriteList = (Button) findViewById(R.id.FavoriteListButton);
        Button vShoppingList = (Button) findViewById(R.id.ShoppingListButton);
        Button mExit = (Button) findViewById(R.id.ExitButton);

        vSearchPage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeToSearch();
            }
        });
        vFavoriteList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeToFavorite();
            }
        });
        vShoppingList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeToShopping();
            }
        });
        mExit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void changeToSearch() {
        Intent SearchView = new Intent(MainMenu.this, SearchPage.class);
        startActivity(SearchView);
    }

    private void changeToFavorite() {
        Intent SearchView = new Intent(MainMenu.this, FavoriteList.class);
        startActivity(SearchView);
    }

    private void changeToShopping() {
        Intent SearchView = new Intent(MainMenu.this, ShoppingList.class);
        startActivity(SearchView);
    }
}
