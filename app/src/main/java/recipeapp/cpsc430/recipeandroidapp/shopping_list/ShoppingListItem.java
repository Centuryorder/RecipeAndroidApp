package recipeapp.cpsc430.recipeandroidapp.shopping_list;

import java.text.Normalizer;

/**
 * ShoppingListItem holds the information of each ingredient from a recipe: the name, the category, and the amount.
 */

public class ShoppingListItem {

    private String name;
    private String category;
    private String imageName;
    private String amount;
    private String unit;

    public ShoppingListItem(String name, String category, String amount, String Unit) {
        this.name = name;
        this.category = category;
        this.amount = amount;
        this.unit = unit;
    }

    public ShoppingListItem(String allinfo) {

            this.name = allinfo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }
    public String getUnit(){return this.unit;}

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAmount() { return amount; }

    public void setAmount(String amount) {
        this.amount = amount;
    }

}
