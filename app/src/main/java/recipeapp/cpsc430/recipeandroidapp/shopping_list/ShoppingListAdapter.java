package recipeapp.cpsc430.recipeandroidapp.shopping_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;

/**
 * ShoppingListAdapter contains the layout that holds the ingredient objects that will be displayed to the user.
 */

public class ShoppingListAdapter extends RecyclerView.Adapter<ShoppingListHolder> {

    private ArrayList<ShoppingListItem> items;
    private Context context;
    private int selection;

    public ShoppingListAdapter(ArrayList<ShoppingListItem> items, Context context, int selection) {

        this.items = items;
        this.context = context;
        this.selection = selection;
    }

    @Override
    public ShoppingListHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemCard = LayoutInflater.from(context).inflate(R.layout.shopping_list_row, parent, false);
        return new ShoppingListHolder(itemCard);
    }

    @Override
    public void onBindViewHolder(final ShoppingListHolder holder, final int position) {

        final ShoppingListItem item = items.get(position);
        holder.updateUI(item, context);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}