package recipeapp.cpsc430.recipeandroidapp.shopping_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.Normalizer;
import java.util.ArrayList;

import recipeapp.cpsc430.recipeandroidapp.R;
import recipeapp.cpsc430.recipeandroidapp.search.RecipeCard;

/**
 * ShoppingList is the activity that allows the shopping list to be displayed to the user with the ingredients from the recipes that they have selected.
 */

public class ShoppingList extends AppCompatActivity {

    public static ArrayList<ShoppingListItem> shoppingListItems = new ArrayList<ShoppingListItem>();
    private ShoppingListAdapter adapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        /** handle swiping of recipe card objects to remove from the shopping list **/
        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            ShoppingListItem item = shoppingListItems.get(viewHolder.getAdapterPosition());
            shoppingListItems.remove(item);
            adapter.notifyItemRemoved(viewHolder.getAdapterPosition());
            adapter.notifyItemRangeChanged(viewHolder.getAdapterPosition(), shoppingListItems.size());
            String displayMessage = item.getName() + " removed from your shopping list.";
            Toast toast = Toast.makeText(getApplicationContext(), displayMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    };

    /**
     * Sets the array list adapter with correct items.
     */
    private void updateItems(int selection) {
        adapter = new ShoppingListAdapter(shoppingListItems, this, selection);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_shopping_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_clear_shopping_list) {
            shoppingListItems.clear();
            updateItems(0);
        } else {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Set back button
        try { getSupportActionBar().setDisplayHomeAsUpEnabled(true); }
        catch (Exception e) { Log.i("Exception", e.getLocalizedMessage()); }

        layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView = findViewById(R.id.shoppingListRecyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new ItemDecorator(0, 0, 0, 10));

        // attach the itemtouchhelper to the recycler view to allow motion gestures like swiping
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);

        updateItems(0);
    }

}
