package recipeapp.cpsc430.recipeandroidapp.shopping_list;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import recipeapp.cpsc430.recipeandroidapp.R;

/**
 * ShoppingListHolder contains the UI for each ingredient to be displayed on the page.
 */

public class ShoppingListHolder extends RecyclerView.ViewHolder {

    private TextView nameLabel;
    private TextView categoryLabel;

    public ShoppingListHolder(View itemView) {
        super(itemView);

        this.nameLabel = itemView.findViewById(R.id.nameLabel);
        this.categoryLabel = itemView.findViewById(R.id.categoryLabel);
    }

    public void updateUI(ShoppingListItem shoppingListItem, Context c) {

        nameLabel.setText(shoppingListItem.getName());
        categoryLabel.setText(shoppingListItem.getCategory());
    }
}